package com.soft.blekotlindemo

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
//import android.companion.CompanionDeviceManager
import android.content.*
import android.content.pm.PackageManager
import android.os.*
//import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.util.*
import kotlin.collections.ArrayList


// https://github.com/objectsyndicate/Kotlin-BluetoothLeGatt/blob/master/Application/src/main/java/com/example/android/bluetoothlegatt/DeviceScanActivity.kt
// coroutine BLE https://github.com/Beepiz/BleGattCoroutines
// https://gist.github.com/search?l=Kotlin&q=kotlin+ble
// https://github.com/Beepiz/BleGattCoroutines
// https://github.com/NordicSemiconductor/Android-BLE-Library/blob/master/ble/src/main/java/no/nordicsemi/android/ble/BleManager.java
// https://developer.android.com/guide/topics/connectivity/companion-device-pairing


@SuppressLint("MissingPermission")
class MainActivity : AppCompatActivity() {
    private val TAG = this::class.java.simpleName

    private val LOCK_DEVICE_NAME = "Thermo 24"
    private val REQUEST_ID_PERMISSIONS = 300
    private val REQUEST_ENABLE_BLE = 400
    private val SPLASH_TIME_OUT = 2000
    private val SCAN_PERIOD = 5 * 60 * 1000L     // 10s


    lateinit var tvMesg: TextView
    lateinit var btnClean: Button
    lateinit var btnSyncDT: Button

    private var mBLEAdapter: BluetoothAdapter? = null
    private var mBLEService: BLEGattService? = null
    private var mConnect = false
    private var bleAddress: String? = null
    private var mScanning: Boolean = false
    private var mHandler: Handler? = null
    private var scanFilter: ArrayList<ScanFilter>?= null
    private var scansetting: ScanSettings? = null
    private var bleScanner:BluetoothLeScanner? = null

    private var fontSize = 0.0f
    private var fontColor = 0





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.w(TAG, "onCreate(), ") 

        mHandler = Handler(Looper.getMainLooper())
    }

    override fun onStart() {
        Log.w(TAG, "onStart(), ")

        initView()
        initControl()

        super.onStart()
    }

    //override fun onRestart() {
    //    Log.w(TAG, "onRestart(), ")
    //    super.onRestart()
    //}

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onResume() {
        super.onResume()
        Log.w(TAG, "onResume(), ")

        // start broadcast Receiver Running.
        registerReceiver(gattUpdateReceiver, gattUpdateFilter(), RECEIVER_EXPORTED)
    }

    val bleResultLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()) { result ->

        if (result.resultCode == Activity.RESULT_CANCELED)
        {
             finish()
        }
    }

    override fun onPause() {
        super.onPause()
        Log.w(TAG, "onPause(), ")
        unregisterReceiver(gattUpdateReceiver)
    }

    override fun onStop() {
        super.onStop()
        Log.w(TAG, "onStop(), ")

    }

    override fun onDestroy() {
        Log.w(TAG, "onDestroy(), ")
        super.onDestroy()
        mBLEService!!.disConnect()
        unbindService(mServiceConnection)
        mBLEService = null
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray )
    {
        Log.w(TAG, "onRequestPermissionsResult(), " +
                "requestCode: $requestCode, grantResults: $grantResults")
        when (requestCode)
        {
            REQUEST_ID_PERMISSIONS -> {
            //  for (i in 0 until grantResults.size - 1 )
            //  {
            //    if (grantResults[i] != )
            //  }
            }

            else ->
            {

            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    @SuppressLint("SetTextI18n")
    fun onClickEvent(view: View)
    {
        Log.w(TAG, " onClickEvent(), view: " + view.id)
        when(view.id) {
            R.id.button01 -> {
                if ((tvMesg.text.length <= "Hello world!!".length)) {
                    tvMesg.text = ""
                } else if (!mScanning) {
                    tvMesg.text = ""
                    startService()
                    bleStart()
                    scanRestriction()
                    scanBLEDevice(true)
                }
            }

            R.id.button02-> {
                //mBLEService!!.cmdConfirmDevice()
                if ((mBLEService != null) && (bleAddress != null)) {
                    //mBLEService!!.cmdWriteDateTimeToDevice()
                    //mBLEService!!.cmdConfirmDevice()
                    mBLEService!!.cmdReadUserIDandFWVersion()
                }
            }
            else ->{

            }
        }
    }




    // ----------------- User define function ------------------- //
    private fun initView()
    {
        Log.w(TAG, " initView(), ")
        tvMesg = findViewById(R.id.textview01)
        btnClean = findViewById(R.id.button01)
        btnSyncDT = findViewById(R.id.button02)
    }

    private fun initControl()
    {
        Log.w(TAG, " initControl(), ")

        tvMesg.movementMethod = ScrollingMovementMethod.getInstance()


        // check BLE feature support
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE))
        {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show()
            finish()
        }

        if (!checkPermission())
        {
            Handler(Looper.getMainLooper()).postDelayed({
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
                finish()
            }, SPLASH_TIME_OUT.toLong() )
        }

        // BLE start Adater
        scanRestriction()
        bleStart()
        scanBLEDevice(true)
        startService()

    }

    private fun checkPermission() : Boolean
    {
        Log.w(TAG, " checkPermission(), ")
        val locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val BLE_AdminPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN)
        val BLEPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH)

        val permissionList = ArrayList<String>()

        if (locationPermission != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION)

        if (BLE_AdminPermission != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.BLUETOOTH_ADMIN)

        if (BLEPermission != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.BLUETOOTH)

        if (!permissionList.isEmpty())
        {
            ActivityCompat.requestPermissions(this, permissionList.toTypedArray(), REQUEST_ID_PERMISSIONS)
            return false
        }

        return true
    }

    private fun scanRestriction()
    {
        Log.w(TAG, " scanRestriction(), ")
        scansetting = ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build()
        //val sf1 = ScanFilter.Builder().setDeviceName(LOCK_DEVICE_NAME).build()
        scanFilter = ArrayList<ScanFilter>()
        //scanFilter!!.add(ScanFilter.Builder().setDeviceName("").build())
        //scanFilter!!.add(sf1)
        //scanFilter!!.add(ScanFilter.Builder().setDeviceName(LOCK_DEVICE_NAME).build())
        //scanFilter!!.add(ScanFilter.Builder().setDeviceName("4G_BLE_1").build())
        //scanFilter!!.add(ScanFilter.Builder().setDeviceName("AP02 AT-UART").build())
        scanFilter!!.add(ScanFilter.Builder().setServiceUuid(ParcelUuid(UUID.fromString(
                                            BLEGattService.UUID_MLC_BLE))) .build())
        //scanFilter!!.add(ScanFilter.Builder().setDeviceName("B6 Connect").build())


        //(scanFilter as ArrayList<ScanFilter>).add(ScanFilter.Builder()
        //            .setDeviceName("LOCK_DEVICE_NAME").build())
    }

    private fun bleStart()
    {
        Log.w(TAG, " bleStart(), ")
        val bleManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBLEAdapter = bleManager.adapter
        bleScanner = bleManager.adapter.bluetoothLeScanner ?: null

        if (mBLEAdapter == null)
        {
            Toast.makeText(this, "Error!! Bluetooth not supported", Toast.LENGTH_SHORT).show()
            finish()
            return
        }
        else if (!mBLEAdapter!!.isEnabled)
        {
            //val enableBLEIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            //startActivityForResult(enableBLEIntent, REQUEST_ENABLE_BLE)
            mBLEAdapter!!.enable()
        }
        //val enableBLEIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        bleResultLauncher.launch( Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE) )
    }



    private fun scanBLEDevice(enable: Boolean)
    {
        Log.w(TAG, " scanBLEDevice(), enable: $enable")
        if (enable)
        {
            mHandler!!.postDelayed({
                mScanning = false
                mBLEAdapter?.bluetoothLeScanner?.stopScan(mBLEScanCallback)
            }, SCAN_PERIOD)

            mScanning = true
            val result = mBLEAdapter!!.bluetoothLeScanner
                                      .startScan(scanFilter, scansetting, mBLEScanCallback)
            Log.w(TAG, " postDelayed(), result: $result")
        }
        else
        {
            if (mHandler != null)
                mHandler!!.removeCallbacksAndMessages(null)

            Log.w(TAG, " BLE Stop scan !! ")
            mScanning = false
            mBLEAdapter?.bluetoothLeScanner?.stopScan(mBLEScanCallback)
        }

    }

    private fun startService()
    {
        Log.w(TAG, " startService(), ")
        val gattServiceIntent = Intent(this, BLEGattService::class.java)
        val serviceState = bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE)

        Log.w(TAG, " startService(), gattServiceIntent: $gattServiceIntent, serviceState: $serviceState")
    }

    //---------------- BLE Scans Callback funtion ----------------- //
    // https://discuss.kotlinlang.org/t/ble-startscan-constantly-returns-null/14404
    private val mBLEScanCallback = object: ScanCallback()
    {
        @RequiresApi(Build.VERSION_CODES.VANILLA_ICE_CREAM)
        @SuppressLint("SetTextI18n")
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            Log.w(TAG, " onScanResult(), mBLEService: $mBLEService, " +
                    "callbackType: $callbackType, result: $result")

            //tvMesg.append(result?.device?.address + ", " +
            //                result?.device?.name + ", " +
            //                result?.device?.bondState + ", " +
            //        result?.device.also {  } + "\n")
            btnClean.text = "Scan"

            if (mBLEService != null)
            {
                tvMesg.text = ""
                val device: BluetoothDevice = result!!.device
                bleAddress = device.address
                tvMesg.append("Address: $bleAddress")
                //var boundState: Boolean
                val  rssi = result.rssi
                //val setPin: ByteArray = byteArrayOf(0,0,0,0,1,2)
                val bondStatus = device.createBond()
                //Log.e(TAG, " onScanResult(), device: ${device.addressType}; ${device.bluetoothClass}; " +
                //        "${device.uuids}; ${device.type}; ${device.connectGatt(this@MainActivity, true, mBLEService!!.mGattCallback)}")

                //when (device.bondState)
                //{
                //    BluetoothDevice.BOND_NONE -> {
                //        bleAddress = device.address
                //        val connectResult = mBLEService!!.connect(bleAddress)
                //        tvMesg.append("connected connectResult: $connectResult \n $bleAddress : ${device.name}: rssi: $rssi \n")
                //        scanBLEDevice(false)
                //    }
                //    else -> {
                //        val bondStatus = device.createBond()
                        //val connectResult = mBLEService!!.connect(bleAddress)
                        val connectResult = mBLEService!!.connect(device.address)
                        tvMesg.append(" bondStatus: $bondStatus, connectResult: $connectResult, " +
                                        "connected\n $bleAddress : ${device.name}: rssi: $rssi \n")
                //        scanBLEDevice(false)
                //    }
                //}
            }
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            Log.w(TAG, " onBatchScanResults(), results: $results")
            super.onBatchScanResults(results)
        }

        override fun onScanFailed(errorCode: Int) {
            Log.w(TAG, " onScanFailed(), errorCode: $errorCode")
            super.onScanFailed(errorCode)
        }
    }

    // ---------------- Service Connection variable ------------------ //
    private val mServiceConnection = object : ServiceConnection
    {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            Log.w(TAG, " onServiceConnected(), name: $name")
            mBLEService = (service as BLEGattService.LocalBinder).service
            if (!mBLEService!!.initialize())
            {
                Log.e(TAG, "Unable to initialize Bluetooth")
                finish()
            }

            //mBLEService!!.connect(bleAddress)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.w(TAG, " onServiceDisconnected(), name: $name")
            mBLEService = null
        }
    }

    // --------------- Broadcast Receiver variable --------------- //
    private val gattUpdateReceiver = object : BroadcastReceiver()
    {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            Log.w(TAG, " onReceive(), action: $action")

            when (action)
            {
                BLEGattService.GATT_CONNECTED-> {
                    tvMesg.append("GATT connected: $action\n")
                    scanBLEDevice(false)
                }

                BLEGattService.GATT_DISCONNECTED-> {
                    tvMesg.append("GATT disConnected: $action\n")
                }

                BLEGattService.GATT_SERVICE_DISCOVERED -> {
                    tvMesg.append("Service discovered: $action\n")
                }

                BLEGattService.GATT_DATA_AVAILABLE-> {
                    val data = intent.getStringExtra(BLEGattService.EXTRA_DATA)
                    tvMesg.append(">> $data\n")
                }

                BLEGattService.MLC_COMMAND_ACTION-> {
                    try {
                        //Thread.sleep(300)
                        mBLEService?.cmdReadUserIDandFWVersion()
                        //mBLEService?.cmdConfirmDevice()
                        //mBLEService?.cmdWriteDateTimeToDevice()
                    }
                    catch(e: InterruptedException)
                    {
                        e.printStackTrace()
                    }
                }

                //BLEGattService.EXTRA_DATA -> {
                //  val data = intent.getStringExtra(BLE)
                //  tvMesg.append("Extra Data: " + action + "\n")
                //}

                else -> {
                    tvMesg.text = " Nothing !!"
                }
            }
        }

    }

    // --------------- Receiver Intent Filter  --------------- //
    private fun gattUpdateFilter(): IntentFilter
    {
        val  intentFilter = IntentFilter()

        intentFilter.addAction(BLEGattService.GATT_CONNECTED)
        intentFilter.addAction(BLEGattService.GATT_DISCONNECTED)
        intentFilter.addAction(BLEGattService.GATT_SERVICE_DISCOVERED)
        intentFilter.addAction(BLEGattService.GATT_DATA_AVAILABLE)


        return intentFilter
    }

    //private val deviceManager: CompanionDeviceManager by lazy(LazyThreadSafetyMode.NONE)
    //{
    //    getSystemService(CompanionDeviceManager::class.java)
    //}


}
