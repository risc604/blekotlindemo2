package com.soft.blekotlindemo

import android.annotation.SuppressLint
import android.app.Service
import android.bluetooth.*
import android.bluetooth.BluetoothAdapter.*
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.webkit.URLUtil
import androidx.annotation.RequiresApi
import java.lang.Exception
import java.util.*
import kotlin.experimental.and

//fun ByteArray.toHex() = this.joinToString(separator = "")
//{
//   it.toInt().and(0xff).toString(16).padStart(2, '0')
//}

//fun String.hexStringToByteArray() = ByteArray(this.length / 2)
//{
//    this.substring(it * 2, it * 2 + 2).toInt(16).toByte()
//}


//SuppressLint("Registered")

/**
 * BLE Gatt Service
 *
 * @constructor Create empty BLE gatt service
 */

@SuppressLint("MissingPermission")
class BLEGattService: Service()
{
    private val TAG = javaClass.simpleName

    private var bleManager: BluetoothManager? = null
    private var bleAdapter: BluetoothAdapter? = null
    private var bleGatt: BluetoothGatt? = null
    private var bleAddress: String? = null
    private var bleConnectSate = STATE_DISCONNECTED
    private var mlcReadCharacteristic: BluetoothGattCharacteristic? = null
    private var mlcWriteCharacteristic: BluetoothGattCharacteristic? = null


    companion object {
        const val GATT_CONNECTED = "com.soft.blekotlindemo.ACTION_GATT_CONNECTED"
        const val GATT_DISCONNECTED = "com.soft.blekotlindemo.ACTION_GATT_DISCONNECTED"
        const val GATT_SERVICE_DISCOVERED = "com.soft.blekotlindemo.ACTION_GATT_SERVICE_DISCOVERED"
        const val GATT_DATA_AVAILABLE = "com.soft.blekotlindemo.ACTION_GATT_DATA_AVAILABLE"
        const val MLC_COMMAND_ACTION = "com.soft.blekotlindemo.ACTION_COMMAND"
        const val EXTRA_DATA = "com.soft.blekotlindemo.EXTRA_DATA"
        const val CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb"
        const val UUID_MLC_BLE = "0000fff0-0000-1000-8000-00805f9b34fb"
        const val UUID_MLC_BLE_READ = "0000fff1-0000-1000-8000-00805f9b34fb"
        const val UUID_MLC_BLE_WRITE = "0000fff2-0000-1000-8000-00805f9b34fb"
        const val UUID_RSL10 = "e093f3b5-00a3-a9e5-9eca-40016e0edc24"
        const val UUID_RSL10_READ = "e093f3b5-00a3-a9e5-9eca-40036e0edc24"
        const val UUID_RSL10_WRITE= "e093f3b5-00a3-a9e5-9eca-40036e0edc24"
    }


    /**
     * On create
     *
     */
    override fun onCreate() {
        super.onCreate()
        Log.w(TAG, "onCreate(), ")

    }

    /**
     * On start command
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.w(TAG, "onStartCommand(), startId: $startId")
        return super.onStartCommand(intent, flags, startId)
    }

    /**
     * On bind
     *
     * @param intent
     * @return
     */
    override fun onBind(intent: Intent?): IBinder? {
        Log.w(TAG, "onBind(), ")
        return mBinder
    }


    override fun onUnbind(intent: Intent?): Boolean {
        Log.w(TAG, "onUnbind(), ")
        close()
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        Log.w(TAG, "onDestroy(), ")
        super.onDestroy()
    }

    //--------------- inner class for get service ------------------//
    inner class LocalBinder: Binder()
    {
        internal val service: BLEGattService
        get() = this@BLEGattService
    }

    private val mBinder = LocalBinder()



    // -------------- Gatt callback --------------- //
    val mGattCallback = object: BluetoothGattCallback()
    {
        @SuppressLint("NewApi")
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            Log.w(TAG, "1 onConnectionStateChange(), status: $status, newState: $newState")
            //super.onConnectionStateChange(gatt, status, newState)
            //gatt!!.requestMtu(23)

            Log.d(TAG, "2 onConnectionStateChange(), gatt: ${gatt?.device!!.name};" +
                    "${gatt?.services?.size}; ${gatt?.device!!.uuids}")

            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {

                    /*** importent !! Real connected external BLE device. ***/
                    val gattServiceState = bleGatt!!.discoverServices()
                    Log.w(TAG, "3 Connected to GATT server, gattServiceState: $gattServiceState")

                    bleConnectSate = STATE_CONNECTED
                    gatt.requestMtu(23)
                    broadcastUpdate(GATT_CONNECTED)

                    when(status) {
                        BluetoothGatt.GATT_INSUFFICIENT_AUTHORIZATION -> {
                            Log.e(TAG, "GATT INSUFFICIENT AUTHORIZATION")
                            //gatt!!.close()
                            gatt!!.device.createBond()
                        }

                        BluetoothGatt.GATT_SUCCESS -> {
                            Log.w(TAG, "onConnectionStateChange(), GATT_SUCCESS, to discovery BLE device services  !!")
                        }

                        BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION -> {
                            gatt!!.device.createBond()
                            Log.w(TAG, "GATT_INSUFFICIENT_AUTHENTICATION ")
                        }
                        BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION ->{
                            gatt!!.device.createBond()
                            Log.w(TAG, "GATT_INSUFFICIENT_ENCRYPTION")
                        }

                        else ->
                        Log.e(TAG, "GATT Stat: $status")
                    }
                }

                BluetoothProfile.STATE_DISCONNECTED -> {
                    bleConnectSate = STATE_DISCONNECTED
                    broadcastUpdate(GATT_DISCONNECTED)
                    Log.e(TAG, "Disconnected from GATT server.")
                }
                else -> {
                    Log.e(TAG, "Error newState: $newState")
                }
            }
        }

        override fun onMtuChanged(gatt: BluetoothGatt?, mtu: Int, status: Int) {
            Log.w(TAG, "onMtuChanged(${gatt!!.device.name}, $mtu, $status), ")
            super.onMtuChanged(gatt, mtu, status)

        }

        @RequiresApi(Build.VERSION_CODES.TIRAMISU)
        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            Log.w(TAG, "onServicesDiscovered(${gatt?.device?.address}, $status )")
            //super.onServicesDiscovered(gatt, status)
            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                if (gatt != null) {
                    bleGatt = gatt
                    findBLECharacteristic(gatt)
                }

                //val characteristic = gatt!!.getService(UUID.fromString(UUID_MLC_BLE))
                //    .getCharacteristic(UUID.fromString(UUID_MLC_BLE_WRITE))

                //gatt!!.setCharacteristicNotification(characteristic, true)
                //broadcastUpdate(GATT_SERVICE_DISCOVERED)
            }
            else
            {
                Log.w(TAG, "onServicesDiscovered received: $status")
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?,
                                       descriptor: BluetoothGattDescriptor?,
                                       status: Int )
        {
            Log.w(TAG, "onDescriptorWrite(${gatt?.services?.size}, ${descriptor?.uuid} , $status)")

            gatt?.services?.forEach { it ->
                println("gatt service: ${it.uuid}")
            }


            if (status == BluetoothGatt.GATT_SUCCESS) {
                cmdConfirmDevice()
                //broadcastUpdate(MLC_COMMAND_ACTION)
            }
            super.onDescriptorWrite(gatt, descriptor, status)
        }

        /* @Deprecated("Deprecated in Java")
        override fun onCharacteristicChanged(gatt: BluetoothGatt?,
                                             characteristic: BluetoothGattCharacteristic? )
        {
            Log.w(TAG, "onCharacteristicChanged(), ")
            //val hexString = characteristic?.value?.toHex()
            //Log.w(TAG, "onCharacteristicChanged(), value: $hexString")
            broadcastUpdate(GATT_DATA_AVAILABLE, characteristic!!)
            super.onCharacteristicChanged(gatt, characteristic)
        }

        @Deprecated("Deprecated in Java")
        override fun onCharacteristicRead(gatt: BluetoothGatt?,
                                          characteristic: BluetoothGattCharacteristic?,
                                          status: Int )
        {
            Log.w(TAG, " onCharacteristicRead(), status: $status")
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //cmdConfirmDevice()
                broadcastUpdate(GATT_DATA_AVAILABLE, characteristic!!)
            }
            super.onCharacteristicRead(gatt, characteristic, status)
        }
         */

        override fun onCharacteristicChanged(gatt: BluetoothGatt,
                                             characteristic: BluetoothGattCharacteristic,
                                             value: ByteArray) {
            Log.v(TAG, "onCharacteristicChanged($gatt, $characteristic, ${value.asHexLower}")

            broadcastUpdate(GATT_DATA_AVAILABLE, characteristic, value, 1)
            super.onCharacteristicChanged(gatt, characteristic, value)
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt,
                                          characteristic: BluetoothGattCharacteristic,
                                          value: ByteArray,
                                          status: Int) {
            Log.v(TAG, " onCharacteristicRead($gatt, ${characteristic.uuid}, " +
                            "${value.asHexUpper}, $status")

            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(GATT_DATA_AVAILABLE, characteristic, value, 2)
            }
            else if ((status == BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION) ||
                     (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) )
            {
                Log.e(TAG, "onCharacteristicRead(), BluetoothDevice.ACTION_BOND_STATE_CHANGED")
                broadcastUpdate(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
            }


            super.onCharacteristicRead(gatt, characteristic, value, status)
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?,
                                           characteristic: BluetoothGattCharacteristic?,
                                           status: Int )
        {
            Log.w(TAG, "onCharacteristicWrite(),status: $status ")
            super.onCharacteristicWrite(gatt, characteristic, status)
        }
    }



    // ---------------- User defin funtion ------------------ //
    fun initialize(): Boolean
    {
        Log.w(TAG, "initialize(), ")
        if (bleManager == null)
        {
            bleManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            if (bleManager == null)
            {
                Log.e(TAG, "Unable to initialize BluetoothManager.")
                return false
            }
        }

        bleAdapter = bleManager!!.adapter
        if (bleAdapter == null)
        {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.")
            return false
        }

        return true
    }


    fun connect(address: String?): Boolean
    {
        Log.w(TAG, "connect(), address: $address")
        if ((bleAdapter == null) || (address == null)) {
            Log.w(TAG, "BluetoothAdapter NOT initialized or upspecified address !!")
            return false
        }

        // try re-connect
        if ((bleAddress != null) && (address == bleAddress) && (bleGatt != null))
        {
            Log.w(TAG, "connect(), Re-Trying to use an existing BLE GATT for re-connection.")
            if (bleGatt!!.connect())
            {
                bleConnectSate = STATE_CONNECTING
                return true
            }
            else
            {
                return false
            }
        }

        val bleDevice = bleAdapter!!.getRemoteDevice(address)
        if (bleDevice == null)
        {
            Log.e(TAG, "Device not found. Unable to connect !!")
            return false
        }
        // connect to ble GATT callback
        bleGatt = bleDevice.connectGatt(this, false, mGattCallback)
        //bleGatt?.requestMtu(23) ?: return false
        bleAddress = address
        bleConnectSate = STATE_CONNECTING

        Log.w(TAG, "connect(), Trying to create a new connection. mtu(23) bond: ${bleDevice.bondState}")
        //val bondStatus = bleDevice.createBond()
        //Log.w(TAG, "connect(),  bondStatus: $bondStatus, bleDevice.bondState: ${bleDevice.bondState}")
        when (bleDevice.bondState)
        {
            BluetoothDevice.BOND_NONE ->{
                //if (bleGatt != null) {
                //    bleGatt!!.close()
                //val bondStatus = bleDevice.createBond()
                //    bleGatt = bleDevice.connectGatt(this, false, mGattCallback)

                //bleGatt = bleDevice.connectGatt(this, false, mGattCallback)
                //Log.w(TAG, "2 connect(),  bondStatus: $bondStatus, bleDevice.bondState: ${bleDevice.bondState}")
                //}
                //close()
                //connect(bleGatt!!.device.address)
                Log.w(TAG, "Not paired !!")
            }

            BluetoothDevice.BOND_BONDING -> {
                //bleDevice.createBond()
            }
            else -> {
                bleGatt = bleDevice.connectGatt(this, false, mGattCallback)
            }
        }
        return true
    }

    fun disConnect()
    {
        Log.w(TAG, "disConnect(), ")
        if ((bleAdapter == null) || (bleGatt == null))
        {
            Log.w(TAG, "BluetoothAdapter not initialized !!")
            return
        }

        bleGatt!!.disconnect()
    }

    // fun setCharacteristicNotification(characteristic: BluetoothGattCharacteristic, enabled: Boolean)
    // {
    //     Log.w(TAG, "setCharacteristicNotification(), characteristic: ${characteristic.uuid}")
    //     if ((bleAdapter == null) || (bleGatt == null))
    //     {
    //         Log.e(TAG, "Bluetooth not initialized.")
    //         return
    //     }
    // 
    //     bleGatt!!.setCharacteristicNotification(characteristic, enabled)
    //     Log.w(TAG, "111 setCharacteristicNotification(), characteristic: ${characteristic.uuid}")
    //     if ((UUID.fromString(UUID_MLC_BLE_READ) == characteristic.uuid) ||
    //         (UUID.fromString(UUID_MLC_BLE_WRITE) == characteristic.uuid)  )
    //     {
    //         Log.w(TAG, "222 setCharacteristicNotification(), characteristic: ${characteristic.uuid}")
    //         val descriptor = characteristic.getDescriptor(
    //                             UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG))
    //         descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
    //         bleGatt!!.writeDescriptor(descriptor)
    //     }
    // }

    @Suppress("DEPRECATION")
    @SuppressLint("NewApi")
    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun findBLECharacteristic(gatt: BluetoothGatt)
    {
        Log.w(TAG, "foundBLECharacteristic(), gatt: ${gatt.getService(UUID.fromString(UUID_MLC_BLE))}")
        val gattSevices = gatt.services

        if (gattSevices == null)
        {
            Log.e(TAG, "gatt Service is NULL !!")
            return
        }

        /*
        for (bleService in gattSevices)
        {
            //Log.w(TAG, "GATT Service: " + bleService.uuid.toString())
            if ((bleService.uuid == UUID.fromString(UUID_MLC_BLE)) ||
                (bleService.uuid == UUID.fromString(UUID_RSL10)))
            {
                val mlcGattCharacters = bleService.characteristics

                for (mlcCharacter in mlcGattCharacters)
                {
                    Log.w(TAG, "new Character: ${mlcCharacter.uuid}")

                    if ((mlcCharacter.uuid == UUID.fromString(UUID_MLC_BLE_READ)) ||
                        (mlcCharacter.uuid == UUID.fromString(UUID_RSL10_READ)) )
                    {
                        bleGatt!!.setCharacteristicNotification(mlcCharacter, true)
                        mlcReadCharacteristic = mlcCharacter
                        //setCharacteristicNotification(mlcCharacter, true)
                        Log.w(TAG, " Read Characteristic writeDescriptor 2902")
                        val descriptor = mlcCharacter.getDescriptor(
                                            UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG))

                        if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.S_V2)
                        {
                            val state = bleGatt!!.writeDescriptor(descriptor, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                            Log.w(TAG, "13 writeDescriptor, state: $state ")
                        }
                        else {
                            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                            val state = bleGatt!!.writeDescriptor(descriptor)
                            Log.w(TAG, "12 writeDescriptor, state: $state ")
                        }

                    }

                    if ((mlcCharacter.uuid == UUID.fromString(UUID_MLC_BLE_WRITE)) ||
                        (mlcCharacter.uuid == UUID.fromString(UUID_RSL10_WRITE)) )
                    {
                        bleGatt!!.setCharacteristicNotification(mlcCharacter, true)   
                        mlcWriteCharacteristic = mlcCharacter
                        //setCharacteristicNotification(mlcCharacter, true)
                        /* try {
                            Thread.sleep(300)

                            Log.w(TAG, " Write Characteristic writeDescriptor 2902")
                            val descriptor = mlcCharacter.getDescriptor(
                                UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG))
                            ///descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                            bleGatt!!.writeDescriptor(descriptor,
                                        BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                        }
                        catch (e: Exception)
                        {
                            e.printStackTrace()
                        } */
                    }
                }

                Log.w(TAG, "Read: ${mlcReadCharacteristic?.uuid} \n" +
                        "Write: ${mlcWriteCharacteristic?.uuid}")
            }
        } */
        val mlcService = gatt.getService(UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb"))
        for (mlcChart in mlcService.characteristics)
        {
            println("mlc: ${mlcChart.uuid}")
            if (mlcChart.uuid.equals(UUID.fromString(UUID_MLC_BLE_READ)))
            {
                mlcReadCharacteristic = mlcChart
                val descriptor = mlcReadCharacteristic!!.getDescriptor(
                    UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG))
                if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.S_V2)
                {
                    val state = bleGatt!!.writeDescriptor(descriptor, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                    Log.w(TAG, "13 writeDescriptor, state: $state ")
                }
                else {
                    descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    val state = bleGatt!!.writeDescriptor(descriptor)
                    Log.w(TAG, "12 writeDescriptor, state: $state ")
                }
            }

            if (mlcChart.uuid.equals(UUID.fromString(UUID_MLC_BLE_WRITE))) {
                mlcWriteCharacteristic = mlcChart
            }

        }

        val hwService = gatt.getService(UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb"))

        for (hwChart in hwService.characteristics)
        {
            println(">>> ${hwChart.uuid}")
        }


    }

    fun cmdConfirmDevice()
    {
        Log.w(TAG, " cmdConfirmDevice(), ")
        val cmd0xA1: ByteArray = byteArrayOf(0x4D, 0xFF.toByte(), 0x00, 0x08, 0x00.toByte(),
            0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(),
            0x0B)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        cmd0xA1[5] = (calendar.get(Calendar.YEAR) - 2000).toByte()
        cmd0xA1[6] = (calendar.get(Calendar.MONTH) + 1).toByte()
        cmd0xA1[7] = (calendar.get(Calendar.DAY_OF_MONTH)).toByte()
        cmd0xA1[8] = (calendar.get(Calendar.HOUR_OF_DAY)).toByte()
        cmd0xA1[9] = (calendar.get(Calendar.MINUTE)).toByte()
        cmd0xA1[10] = (calendar.get(Calendar.SECOND)).toByte()
        Log.w(TAG, " cmd0xA1: " +  cmd0xA1.toHexString())

        //val macAddr = bleAddress?.let { removeColon(it) }
        //val byteAddr = macAddr?.byteArrayFromHexString()
        //cmd0xA1[13] = byteAddr!!.get(0)
        //cmd0xA1[14] = byteAddr.get(1)
        //cmd0xA1[15] = byteAddr.get(2)
        //cmd0xA1[16] = byteAddr.get(3)
        //cmd0xA1[17] = byteAddr.get(4)
        //cmd0xA1[18] = byteAddr.get(5)
        Log.w(TAG, "cmd0xA1: " + cmd0xA1.toHexString())

        var cs=0
        for (i in 0 until (cmd0xA1.size-1) step 1 )
        {
            cs += cmd0xA1.get(i)
        }
        cmd0xA1.set(cmd0xA1.size-1, (cs.toByte() and 0xFF.toByte()))

        Log.w(TAG, "cs: ${cs.toString(16)}, cmd0xA1: " + cmd0xA1.toHexString())

        if (mlcWriteCharacteristic != null) {
            mlcWriteCharacteristic?.setValue(cmd0xA1)
            bleGatt?.writeCharacteristic(mlcWriteCharacteristic)
        }
        else
        {
            Log.w(TAG, "Error!! mlcWriteCharacteristic: $mlcWriteCharacteristic")
        }
    }

    fun cmdConfirmDevice_old()
    {
        Log.w(TAG, " cmdConfirmDevice(), ")
        val cmd0xA1: ByteArray = byteArrayOf(0x4D, 0xFF.toByte(), 0x00, 0x14, 0xA1.toByte(),
            0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(),
            0x0B, 0x00,
            0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(),
            0x00)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        cmd0xA1[5] = (calendar.get(Calendar.YEAR) - 2000).toByte()
        cmd0xA1[6] = (calendar.get(Calendar.MONTH) + 1).toByte()
        cmd0xA1[7] = (calendar.get(Calendar.DAY_OF_MONTH)).toByte()
        cmd0xA1[8] = (calendar.get(Calendar.HOUR_OF_DAY)).toByte()
        cmd0xA1[9] = (calendar.get(Calendar.MINUTE)).toByte()
        cmd0xA1[10] = (calendar.get(Calendar.SECOND)).toByte()
        Log.w(TAG, " cmd0xA1: " +  cmd0xA1.toHexString())

        val macAddr = bleAddress?.let { removeColon(it) }
        val byteAddr = macAddr?.byteArrayFromHexString()
        cmd0xA1[13] = byteAddr!![0]
        cmd0xA1[14] = byteAddr[1]
        cmd0xA1[15] = byteAddr[2]
        cmd0xA1[16] = byteAddr[3]
        cmd0xA1[17] = byteAddr[4]
        cmd0xA1[18] = byteAddr[5]
        Log.w(TAG, "cmd0xA1: " + cmd0xA1.toHexString())

        var cs=0
        for (i in 0 until (cmd0xA1.size-1) step 1 )
        {
            cs += cmd0xA1.get(i)
        }
        cmd0xA1.set(cmd0xA1.size-1, (cs.toByte() and 0xFF.toByte()))

        Log.w(TAG, "cs: ${cs.toString(16)}, cmd0xA1: " + cmd0xA1.toHexString())

        if (mlcWriteCharacteristic != null) {
            mlcWriteCharacteristic?.setValue(cmd0xA1)
            bleGatt?.writeCharacteristic(mlcWriteCharacteristic)
        }
        else
        {
            Log.w(TAG, "Error!! mlcWriteCharacteristic: $mlcWriteCharacteristic")
        }
    }

    fun cmdWriteDateTimeToDevice()
    {
        val mlcWrtChart = mlcWriteCharacteristic
        Log.w(TAG, " cmdConfirmDevice(), ")
        val cmdArray: ByteArray = byteArrayOf(0x4D, 0xFF.toByte(), 0x00, 0x08, 0x0D,
            0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(),
            0x0B, 0x00,
            0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(), 0xFF.toByte(),
            0x00)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        cmdArray[5] = (calendar.get(Calendar.YEAR) - 2000).toByte()
        cmdArray[6] = (calendar.get(Calendar.MONTH) + 1).toByte()
        cmdArray[7] = (calendar.get(Calendar.DAY_OF_MONTH)).toByte()
        cmdArray[8] = (calendar.get(Calendar.HOUR_OF_DAY)).toByte()
        cmdArray[9] = (calendar.get(Calendar.MINUTE)).toByte()
        cmdArray[10] = (calendar.get(Calendar.SECOND)).toByte()
        Log.w(TAG, " cmdArray: " +  cmdArray.toHexString())

        val macAddr = bleAddress?.let { removeColon(it) }
        val byteAddr = macAddr?.byteArrayFromHexString()
        cmdArray[13] = byteAddr!!.get(0)
        cmdArray[14] = byteAddr.get(1)
        cmdArray[15] = byteAddr.get(2)
        cmdArray[16] = byteAddr.get(3)
        cmdArray[17] = byteAddr.get(4)
        cmdArray[18] = byteAddr.get(5)
        Log.w(TAG, "cmdArray: " + cmdArray.toHexString())

        var cs=0
        for (i in 0 until (cmdArray.size-1) step 1 )
        {
            cs += cmdArray.get(i)
        }
        cmdArray.set(cmdArray.size-1, (cs.toByte() and 0xFF.toByte()))

        Log.w(TAG, "cs: ${cs.toString(16)}, cmdArray: " + cmdArray.toHexString())

        if (mlcWrtChart != null) {
            mlcWrtChart.setValue(cmdArray)
            bleGatt?.writeCharacteristic(mlcWrtChart)
        }
        else
        {
            Log.w(TAG, "Error!! mlcWriteCharacteristic: $mlcWrtChart")
        }
    }

    fun cmdReadUserIDandFWVersion()     // A6 BT Command 0x05
    {
        val mlcWrtChart = mlcWriteCharacteristic
        Log.w(TAG, " cmdConfirmDevice(), ")
        val cmdArray = byteArrayOf(0x4D, 0xFF.toByte(), 0x00, 0x02, 0x05,
            0xFF.toByte())

        Log.w(TAG, "cmdArray: " + cmdArray.toHexString())

        var cs=0
        for (i in 0 until (cmdArray.size-1) step 1 )
        {
            cs += cmdArray.get(i)
        }
        cmdArray.set(cmdArray.size-1, (cs.toByte() and 0xFF.toByte()))

        Log.w(TAG, "cs: ${cs.toString(16)}, cmdArray: " + cmdArray.toHexString())

        if (mlcWrtChart != null) {
            mlcWrtChart.setValue(cmdArray)
            bleGatt?.writeCharacteristic(mlcWrtChart)
        }
        else
        {
            Log.w(TAG, "Error!! mlcWriteCharacteristic: $mlcWrtChart")
        }
    }



    private fun broadcastUpdate(action: String)
    {
        Log.w(TAG, "1 broadcastUpdate(), action: $action")
        sendBroadcast(Intent(action))
    }

    private fun broadcastUpdate(action: String, characteristic: BluetoothGattCharacteristic)
    {

        val data = characteristic.value.toHex()

        if (data.isNotEmpty())
            run {
                Log.w(TAG, "broadcastUpdate(), data: $data")
                val intent = Intent(action)
                intent.putExtra(EXTRA_DATA, data)
                sendBroadcast(intent)
            }
    }

    private fun broadcastUpdate(action: String,
                                characteristic: BluetoothGattCharacteristic,
                                values: ByteArray, order: Int)
    {
        //val data = values.toHex()
        Log.v(TAG, "2 broadcastUpdate($order),")

        if (values.isNotEmpty())
            run {
                Log.w(TAG, "broadcastUpdate(), data: ${values.toHexString()}")
                val intent = Intent(action)
                intent.putExtra(EXTRA_DATA, values.toHex())
                sendBroadcast(intent)
            }
    }

    private fun close()
    {
        if (bleGatt == null)
        {
            return
        }

        bleGatt!!.close()
        bleGatt = null
    }


}



