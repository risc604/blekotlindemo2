package com.soft.blekotlindemo

import android.content.ContentValues.TAG
import android.util.Log
import java.util.*

object Utils {
    private val TAG = javaClass.simpleName
}


fun ByteArray.toHex(): String {
    //Log.w(TAG, "ByteArray.toHex(), ")
    return joinToString("") { "%02X".format(it.toInt() and 0xFF) }
}

fun ByteArray.toHexString() = joinToString ("")
{
    //Log.w(TAG, "ByteArray.toHexString(), ")
    "%02X".format(it.toInt() and 0xFF)
}


private val HEX_CHARS = "0123456789ABCDEF"
fun String.hexStringToByteArray(): ByteArray
{
    val result = ByteArray(length / 2)

    for (i in 0 until length step 2)
    {
        val firstIndex = HEX_CHARS.indexOf(this[i])
        val secondIndex =  HEX_CHARS.indexOf(this[i+1])
        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    //Log.w(TAG, "String.hexStringToByteArray(), result: $result")
    return result
}

fun String.byteArrayFromHexString() = this.chunked(2).map{
                                        //Log.w(TAG, "String.byteArrayFromHexString(), ")
                                        it.uppercase(Locale.getDefault()).toInt(16).toByte() }.toByteArray()

val ByteArray.asHexLower inline get() = this.joinToString(separator = "")
{
    //Log.w(TAG, "ByteArray.asHexLower inline, ")
    //String.format("%02x", (it.toInt() and 0xFF))
    String.toString()
}

val ByteArray.asHexUpper inline get() = this.joinToString(separator = "")
{
    //Log.w(TAG, "2 ByteArray.asHexUpper inline, ")
    String.format("%02X", (it.toInt() and 0xFF))
}

val String.hexAsByteArray inline get() = this.chunked(2).map {
    //Log.w(TAG, "hexAsByteArray() inline, ")
    it.uppercase(Locale.getDefault()).toInt(16).toByte() }.toByteArray()

fun removeColon(s: String): String
{
    val strArray = s.replace("\\s?:".toRegex(), "")
    //Log.w(TAG, "removeColon(), s: $s, strArray: $strArray")
    return strArray
}


